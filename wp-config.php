<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'bitnami_wordpress');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'bn_wordpress');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', '236be13d2b');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost:3306');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'Gdx_XL5U$[`ApjVCnL b^)[KZ?sx_TNX0W3LVELkNdp-95LV/J;r.9n]F%>ns:jO');
define('SECURE_AUTH_KEY', 'J?{cyRL^:Gd!ETk*~jZ6Fq*?Rn%,[VEnn&aFvW#jL|fxAkyBG3drdB%3WfZUL@Va');
define('LOGGED_IN_KEY', 'J066d&xeXtz8b0q+$[nV {7{myb&m`{<Aa]Yu=NCia<9c;<]-bVoi-nlUYo&-/9x');
define('NONCE_KEY', 'IXzhGh_!z?Q. zP }G</ut{U[i]f?gG8(:zWiC`F>MdRT!NRI:Y??2g?-*EU}J>x');
define('AUTH_SALT', '70sDbCNzU!,[0-B|{|Uem^a%F5Oj_f2,:Ge4pcK;xaDbq{T)hVSbknj~CSSv+bR-');
define('SECURE_AUTH_SALT', 'Zcicsb67>fpnA.6vv<g,b-bQ%~z;@UKm_w$%qs(q(9-NRmzo ^$lw$r$|!wBnekZ');
define('LOGGED_IN_SALT', ';#ds5q>$C/,qJg@39X..NAjU%v(.p>/ln(BtBARuSV_R{ EtmIiM18}mTbha4Lex');
define('NONCE_SALT', '5|3 }N^EB[OgWZis4Cg0=p .y`v0L$r|FC;70~vUll(]w~5_cIyZ/J)stuRvT]2f');

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';


/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

